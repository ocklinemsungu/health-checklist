@extends('layouts.app')
@push('css')

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<link  href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="path/from/html/page/to/jquery.min.js"></script>

</head>
@endpush
@section('custom-style')
<style type="text/css">

  button {
    font-weight: bold;
  }
  button.fa-play-circle-o {
    background-color: lightsalmon;
  }
  button.fa-stop {
    background-color: lightgreen;
  }
  #myCan {
    ...
    pointer-events: none;
  }
</style>
@endsection
@section('content') 

@include('layouts.headers.weak')

<div class="container-fluid mt--3">
  <div class="card bg-secondary shadow">
    <div class="card-header bg-white border-0">
      <div class="pt-3 card-body">
       <div class="row mt-5">
        <div class="col-xl-7 mb-5 mb-xl-0">
          <div class="card shadow">
            <div class="card-header border-0">
              <div class="row align-items-center">
                <div class="col">
                  <h3 class="mb-0">AVAILABILITY OF SERVICE </h3>
                </div>
              </div>
            </div>
            {{--  <form  action="{{ url('/store')}}" method=post enctype="multipart/form-data">
              <input name="_token" type="hidden" value="{{ csrf_token() }}"> --}}
              {{-- {!!Form::open(['action' => 'WeaklyController@store','method' => 'POST', 'enctype' =>'multipart/form-data'])!!} --}}
              {{-- {{ csrf_field() }} --}}
              <div class="table-responsive">
                <!-- Projects table -->
                <table class="table align-items-center table-flush">
                  {!!Form::open(['action' => 'WeaklyController@store','method' => 'POST', 'enctype' =>'multipart/form-data'])!!}
                  {{ csrf_field() }}
                  <thead class="thead-light">
                   <tr>
                     <th scope="col">Service Name</th>
                     <th scope="col">UP/Pass</th>
                     <th scope="col">DOWN/FAIL</th>
                     <th scope="col">N/A</th>
                     <th scope="col">Remark(s)</th>
                   </tr>
                 </thead>
                 <tbody>
                   <tr>
                    <th scope="row">
                      vCenter Server
                    </th>
                    <td><input type="submit" id="done" onclick="submitButtonStyle(this)"  name="vCenter_Server" value="Done-No Issues" class="stylebutton myCan"></td>
                    <td><input type="submit" id="issues" onclick="submitIssues(this)" name="vCenter_Server" class="stylebutton myCan" value="Issue Found"></td>
                    <td><input type="submit" id="NA" name="vCenter_Server" onclick="submitNA(this)" value="N/A" class="stylebutton myCan" style="width:100px;"></td>
                    <td><textarea id="" name="remark" value="" class="stylebutton myCan" style="width:100px;" ></textarea></td>
                  </tr>

                  <tr>
                    <th scope="row">
                      Internet
                    </th>
                    <td><input type="submit" id="done1" onclick="submitButtonStyle(this)"  name="internet" value="Done-No Issues" class="stylebutton myCan"></td>
                    <td><input type="submit" id="issues1" onclick="submitIssues(this)" name="internet" class="stylebutton myCan" value="Issue Found"></td>
                    <td><input type="submit" id="NA1" name="internet" onclick="submitNA(this)" value="N/A" class="stylebutton myCan" style="width:100px;"></td>
                    <td><textarea id="" name="remark" value="" class="stylebutton myCan" style="width:100px;" ></textarea></td>
                  </tr>
                  <tr>
                    <th scope="row">
                      DHCP and DNS
                    </th>
                    <td><input type="submit" id="done2" onclick="submitButtonStyle(this)"  name="DHCP_DNS" value="Done-No Issues" class="stylebutton myCan"></td>
                    <td><input type="submit" id="issues2" onclick="submitIssues(this)" name="DHCP_DNS" class="stylebutton myCan" value="Issue Found"></td>
                    <td><input type="submit" id="NA2" name="DHCP_DNS" onclick="submitNA(this)" value="N/A" class="stylebutton myCan" style="width:100px;"></td>
                    <td><textarea id="" name="remark" value="" class="stylebutton myCan" style="width:100px;" ></textarea></td>
                  </tr>

                  <tr>
                    <th scope="row">
                      document Share
                    </th>
                    <td><input type="submit" id="done3" onclick="submitButtonStyle(this)"  name="document_share" value="Done-No Issues" class="stylebutton myCan"></td>
                    <td><input type="submit" id="issues3" onclick="submitIssues(this)" name="document_share" class="stylebutton myCan" value="Issue Found"></td>
                    <td><input type="submit" id="NA3" name="document_share" onclick="submitNA(this)" value="N/A" class="stylebutton myCan" style="width:100px;"></td>
                    <td><textarea id="" name="remark" value="" class="stylebutton myCan" style="width:100px;" ></textarea></td>
                  </tr>

                  <tr>
                    <th scope="row">
                      Send and Receive Internal Email
                    </th>
                    <td><input type="submit" id="done4" onclick="submitButtonStyle(this)"  name="send_receive_int_mail" value="Done-No Issues" class="stylebutton myCan"></td>
                    <td><input type="submit" id="issues4" onclick="submitIssues(this)" name="send_receive_int_mail" class="stylebutton myCan" value="Issue Found"></td>
                    <td><input type="submit" id="NA4" name="send_receive_int_mail" onclick="submitNA(this)" value="N/A" class="stylebutton myCan" style="width:100px;"></td>
                    <td><textarea id="" name="remark" value="" class="stylebutton myCan" style="width:100px;" ></textarea></td>
                  </tr>

                  <tr>
                    <th scope="row">
                      Send and Receive External Email
                    </th>
                    <td><input type="submit" id="done5" onclick="submitButtonStyle(this)"  name="send_receive_ext_mail" value="Done-No Issues" class="stylebutton myCan"></td>
                    <td><input type="submit" id="issues5" onclick="submitIssues(this)" name="send_receive_ext_mail" class="stylebutton myCan" value="Issue Found"></td>
                    <td><input type="submit" id="NA5" name="send_receive_ext_mail" onclick="submitNA(this)" value="N/A" class="stylebutton myCan" style="width:100px;"></td>
                    <td><textarea id="" name="remark" value="" class="stylebutton myCan" style="width:100px;" ></textarea></td>
                  </tr>

                  <tr>
                    <th scope="row">
                      MAC
                    </th>
                    <td><input type="submit" id="done6" onclick="submitButtonStyle(this)"  name="MAC" value="Done-No Issues" class="stylebutton myCan"></td>
                    <td><input type="submit" id="issues6" onclick="submitIssues(this)" name="MAC" class="stylebutton myCan" value="Issue Found"></td>
                    <td><input type="submit" id="NA6" name="MAC" onclick="submitNA(this)" value="N/A" class="stylebutton myCan" style="width:100px;"></td>
                    <td><textarea id="" name="remark" value="" class="stylebutton myCan" style="width:100px;" ></textarea></td>
                  </tr>

                  <tr>
                    <th scope="row">
                      e-Office
                    </th>
                    <td><input type="submit" id="done7" onclick="submitButtonStyle(this)"  name="e-Office" value="Done-No Issues" class="stylebutton myCan"></td>
                    <td><input type="submit" id="issues7" onclick="submitIssues(this)" name="e-Office" class="stylebutton myCan" value="Issue Found"></td>
                    <td><input type="submit" id="NA7" name="e-Office" onclick="submitNA(this)" value="N/A" class="stylebutton myCan" style="width:100px;"></td>
                    <td><textarea id="" name="remark" value="" class="stylebutton myCan" style="width:100px;" ></textarea></td>
                  </tr>

                  <tr>
                    <th scope="row">
                      Intranet
                    </th>
                    <td><input type="submit" id="done8" onclick="submitButtonStyle(this)"  name="intranet" value="Done-No Issues" class="stylebutton myCan"></td>
                    <td><input type="submit" id="issues8" onclick="submitIssues(this)" name="intranet" class="stylebutton myCan" value="Issue Found"></td>
                    <td><input type="submit" id="NA8" name="intranet" onclick="submitNA(this)" value="N/A" class="stylebutton myCan" style="width:100px;"> </td>
                    <td><textarea id="" name="remark" value="" class="stylebutton myCan" style="width:100px;" ></textarea></td>
                  </tr>

                  <tr>
                    <th scope="row">
                      Public Website(www.wcf.go.tz)
                    </th>
                    <td><input type="submit" id="done9" onclick="submitButtonStyle(this)"  name="public_website" value="Done-No Issues" class="stylebutton myCan"></td>
                    <td><input type="submit" id="issues9" onclick="submitIssues(this)" name="public_website" class="stylebutton myCan" value="Issue Found"></td>
                    <td><input type="submit" id="NA9" name="public_website" onclick="submitNA(this)" value="N/A" class="stylebutton myCan" style="width:100px;"></td>
                    <td><textarea id="" name="remark" value="" class="stylebutton myCan" style="width:100px;" ></textarea></td>
                  </tr>

                  <tr>
                    <th scope="row">
                      WCFePG
                    </th>
                    <td><input type="submit" id="done10" onclick="submitButtonStyle(this)"  name="WCFePG" value="Done-No Issues" class="stylebutton myCan"></td>
                    <td><input type="submit" id="issues10" onclick="submitIssues(this)" name="WCFePG" class="stylebutton myCan" value="Issue Found"></td>
                    <td><input type="submit" id="NA10" name="WCFePG" onclick="submitNA(this)" value="N/A" class="stylebutton myCan" style="width:100px;"></td>
                    <td><textarea id="" name="remark" value="" class="stylebutton myCan" style="width:100px;" ></textarea></td>
                  </tr>

                  <tr>
                    <th scope="row">
                      Abacus/HRS
                    </th>
                    <td><input type="submit" id="done11" onclick="submitButtonStyle(this)"  name="abacus_HR" value="Done-No Issues" class="stylebutton myCan"></td>
                    <td><input type="submit" id="issues11" onclick="submitIssues(this)" name="abacus_HR" class="stylebutton myCan" value="Issue Found"></td>
                    <td><input type="submit" id="NA11" name="abacus_HR" onclick="submitNA(this)" value="N/A" class="stylebutton myCan" style="width:100px;"></td>
                    <td><textarea id="" name="remark" value="" class="stylebutton myCan" style="width:100px;" ></textarea></td>
                  </tr>

                  <tr>
                    <th scope="row">
                      ERP
                    </th>
                    <td><input type="submit" id="done12" onclick="submitButtonStyle(this)"  name="ERP" value="Done-No Issues" class="stylebutton myCan"></td>
                    <td><input type="submit" id="issues12" onclick="submitIssues(this)" name="ERP" class="stylebutton myCan" value="Issue Found"></td>
                    <td><input type="submit" id="NA12" name="ERP" onclick="submitNA(this)" value="N/A" class="stylebutton myCan" style="width:100px;"></td>
                    <td><textarea id="" name="remark" value="" class="stylebutton myCan" style="width:100px;" ></textarea></td>
                  </tr>

                  <tr>
                    <th scope="row">
                      Time and Attendance System
                    </th>
                    <td><input type="submit" id="done13" onclick="submitButtonStyle(this)"  name="time_attendance" value="Done-No Issues" class="stylebutton myCan"></td>
                    <td><input type="submit" id="issues13" onclick="submitIssues(this)" name="time_attendance" class="stylebutton myCan" value="Issue Found"></td>
                    <td><input type="submit" id="NA13" name="time_attendance" onclick="submitNA(this)" value="N/A" class="stylebutton myCan" style="width:100px;"></td>
                    <td><textarea id="" name="remark" value="" class="stylebutton myCan" style="width:100px;" ></textarea></td>
                  </tr>
                  
                </tbody>
                {!! Form::close() !!}
              </table>
            </div>
          </div>
        </div>

        <div class="col-xl-5">
         <div class="card shadow">
           <div class="card-header border-0">
             <div class="row align-items-center">
               <div class="col">
                 <h3 class="mb-0">General Condition of Server Room(HQ)</h3>
               </div>
             </div>
           </div>
           <div class="table-responsive">
             <!-- Projects table -->
             <table class="table align-items-center table-flush">
               <thead class="thead-light">
                 <tr>
                   <th scope="col">DEVICE NAME</th>
                   <th scope="col">OK</th>
                   <th scope="col">NOT OK</th>
                   <th scope="col">Remark(s)</th>
                 </tr>
               </thead>
               <tbody>
                 <tr>
                   <th scope="row">
                     Air Contitioning(AC)
                   </th>
                   <td><input type="submit" id="done1" onclick="submitButtonStyle(this)"  name="air_condition" value="Done-No Issues" class="stylebutton myCan"></td>
                   <td><input type="submit" id="issues1" onclick="submitIssues(this)" name="air_condition" class="stylebutton myCan" value="Issue Found"></td>
                   <td><textarea id="" name="remark" value="" class="stylebutton myCan" style="width:100px;" ></textarea></td>
                 </tr>

                 <tr>
                   <th scope="row">
                     Server Room UPs
                   </th>
                   <td><input type="submit" id="done1" onclick="submitButtonStyle(this)"  name="server_room_ups" value="Done-No Issues" class="stylebutton myCan"></td>
                   <td><input type="submit" id="issues1" onclick="submitIssues(this)" name="server_room_ups" class="stylebutton myCan" value="Issue Found"></td>
                   <td><textarea id="" name="remark" value="" class="stylebutton myCan" style="width:100px;" ></textarea></td>
                 </tr>

               </tbody>
             </table>
           </div>
         </div>
         {{-- <br/> --}}
         <div class="card shadow">
          <div class="card-header border-0">
           <div class="row align-items-center">
             <div class="col">
               <h3 class="mb-0">General Condition of Server Room(DODOMA)</h3>
             </div>
           </div>
         </div>
         <div class="table-responsive">
           <table class="table align-items-center table-flush">
             <thead class="thead-light">
               <tr>
                 <th scope="col">DEVICE NAME</th>
                 <th scope="col">OK</th>
                 <th scope="col">NOT OK</th>
                 <th scope="col">Remrk(s)</th>
               </tr>
             </thead>
             <tbody>
               <tr>
                 <th scope="row">
                   Air Contitioning(AC)
                 </th>
                 <td><input type="submit" id="done1" onclick="submitButtonStyle(this)"  name="air_condition_1" value="Done-No Issues" class="stylebutton myCan"></td>
                 <td><input type="submit" id="issues1" onclick="submitIssues(this)" name="air_condition_1" class="stylebutton myCan" value="Issue Found"></td>
                 <td><textarea id="" name="remark" value="" class="stylebutton myCan" style="width:100px;" ></textarea></td>
               </tr>

               <tr>
                 <th scope="row">
                   Server Room UPs
                 </th>
                 <td><input type="submit" id="done1" onclick="submitButtonStyle(this)"  name="server_room_ups_1" value="Done-No Issues" class="stylebutton myCan"></td>
                 <td><input type="submit" id="issues1" onclick="submitIssues(this)" name="server_room_ups_1" class="stylebutton myCan" value="Issue Found"> </td>
                 <td><textarea id="" name="remark" value="" class="stylebutton myCan" style="width:100px;" ></textarea></td>
               </tr>

             </tbody>
           </table>
         </div>
       </div>

       <div class="card shadow">
         <div class="card-header border-0">
           <div class="row align-items-center">
             <div class="row col-md-12">
               <div class="col-md-12">
                 <label class="required">Remark</label>
                 <div class="form-group text_content">
                   <textarea name="remark" class="form-control" style="overflow: hidden; word-wrap: break-word; resize: horizontal; !important"></textarea>
                   {{-- <$errors->first('remark', '<span class="help-block label label-danger">:message</span>') > --}}
                 </div>  
               </div>
             </div>
           </div>
         </div> 

         <div class=" row col-md-12">
           <div class="col-md-6">
             <div class="form-group text_content">
               <label><h3 class="mb-0">Verified By:</h3></label>
               <input type="text" name="{{ auth()->user()->name }}" value="{{ auth()->user()->name }}" class="form-control" cols="50">
             </div>
           </div>
           <div class="col-md-6">
             <div class="form-group text_content">
               <label><h3 class="mb-0">Verifier Signature:</h3></label>
               <input type="file" name="signature" class="form-control">
             </div>
           </div>
         </div>

         <div class="row col-md-12">
          <div class="col-md-6">
           <div class="form-group text_content">

           </div> 
         </div>
         <div class="col-md-6">
          <div class="form-group text_content">
            <div class="form-group">
              <button type="button" id="cancel" name="cancel" class="btn btn-danger btn-sm ">Cancel</button>
              <button type="button" id="submit" name="submit" class="btn btn-success btn-sm">Submit</button>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>
</div>
</div>
</div>





@include('layouts.footers.auth')
</div>
@endsection

@push('js')
<script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
<script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"> 

@endpush
<script type="text/javascript">
  // document.addEventListener('DOMContentLoaded', function () {
  //  let result = document.querySelector('#result');
  //  document.body.addEventListener('change', function (e) {
  //   let target = e.target;
  //   let message;
  //   switch (target.id) {
  //     case 'done':
  //     message = 'The Done-No Issues Found';
  //     break;
  //     case 'issues':
  //     message = 'The Issues Found';
  //     break;
  //     case 'NA':
  //     message = 'The NA ';
  //     break;
  //   }
  //   result.textContent = message;
  // });


  function submitButtonStyle(_this) {
    let done = $("a.switchTab.active").attr('href');
    _this.style.backgroundColor = "green";
  }
  function submitIssues(_this) {
    let issues = $("a.switchTab.active").attr('href');
    _this.style.backgroundColor = "red";
  }
  function submitNA(_this) {
    let NA = $("a.switchTab.active").attr('href');
    _this.style.backgroundColor = "yellow";
  }

 // });
</script>













    {{-- </script> --}}