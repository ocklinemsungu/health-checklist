@extends('layouts.app')

@section('custom-style')

<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<style type="text/css">

  button {
    font-weight: bold;
  }
  button.fa-play-circle-o {
    background-color: lightsalmon;
  }
  button.fa-stop {
    background-color: lightgreen;
  }
</style>
@endsection
@section('content') 

@include('layouts.headers.daily')

<div class="container-fluid mt--3">
  <div class="card bg-secondary shadow">
    <div class="card-header bg-white border-0">
      <div class="pt-3 card-body">
       <div class="row mt-5">
        <div class="col-xl-7 mb-5 mb-xl-0">
          <div class="card shadow">
            <div class="card-header border-0">
              <div class="row align-items-center">
                <div class="col">
                  <h3 class="mb-0">AVAILABILITY OF SERVICE </h3>
                </div>
              </div>
            </div>
            <form method="POST" id="myform" action="{{action('DailyController@store')}}" enctype="multipart/form-data">
             <input type="hidden" name="_method"  />
             {{ csrf_field() }}

             <div class="table-responsive">
              <!-- Projects table -->
              <table class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th scope="col">Service Name</th>
                    <th scope="col">UP/Pass</th>
                    <th scope="col">DOWN/FAIL</th>
                    <th scope="col">N/A</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th scope="row">
                      vCenter Server
                    </th>
                    <td>
                      

                     <input type="submit" id="done" onclick="submitButtonStyle(this)"  name="vCenter Server" value="Done-No Issues" class="stylebutton myCan">
                   </td>
                   <td>
                    <input type="submit" id="issues" onclick="submitIssues(this)" name="vCenter Server"  class="stylebutton myCan" value="Issue Found">

                  </td>
                  <td>
                    <input type="submit" id="NA" onclick="submitNA(this)" name="vCenter Server" value="N_A" class="stylebutton myCan" style="width:100px;">

                  </td>
                </tr>
                <tr>
                  <th scope="row">
                    Internet
                  </th>
                  <td>
                   <input type="button" id="done1" onclick="submitButtonStyle(this)"  name="status" value="Done-No Issues" class="stylebutton myCan">
                 </td>
                 <td>
                  <input type="submit" id="issues1" onclick="submitIssues(this)" name="status" class="stylebutton myCan" value="Issue Found">
                </td>
                <td>
                  <input type="submit" id="NA1" name="status" onclick="submitNA(this)" value="N/A" class="stylebutton myCan" style="width:100px;">
                </td>

              </tr>
              <tr>
                <th scope="row">
                 DHCP and DNS
               </th>
               <td>
                 <input type="button" id="done2" onclick="submitButtonStyle(this)"  name="status" value="Done-No Issues" class="stylebutton myCan">
               </td>
               <td>
                <input type="submit" id="issues2" onclick="submitIssues(this)" name="status" class="stylebutton myCan" value="Issue Found">
              </td>
              <td>
                <input type="submit" id="NA2" name="status" onclick="submitNA(this)" value="N/A" class="stylebutton myCan" style="width:100px;">
              </td>
            </tr>
            <tr>
              <th scope="row">
                document Share
              </th>
              <td>
               <input type="button" id="done3" onclick="submitButtonStyle(this)"  name="status" value="Done-No Issues" class="stylebutton myCan">
             </td>
             <td>
              <input type="submit" id="issues3" onclick="submitIssues(this)" name="status" class="stylebutton myCan" value="Issue Found">
            </td>
            <td>
              <input type="submit" id="NA3" name="status" onclick="submitNA(this)" value="N/A" class="stylebutton myCan" style="width:100px;">
            </td>
          </tr>
          <tr>
            <th scope="row">
              Send and Receive Internal Email
            </th>
            <td>
             <input type="button" id="done4" onclick="submitButtonStyle(this)"  name="status" value="Done-No Issues" class="stylebutton myCan">
           </td>
           <td>
            <input type="submit" id="issues4" onclick="submitIssues(this)" name="status" class="stylebutton myCan" value="Issue Found">
          </td>
          <td>
            <input type="submit" id="NA4" name="status" onclick="submitNA(this)" value="N/A" class="stylebutton myCan" style="width:100px;">
          </td>
        </tr>
        <tr>
          <th scope="row">
            Send and Receive External Email
          </th>
          <td>
           <input type="button" id="done5" onclick="submitButtonStyle(this)"  name="status" value="Done-No Issues" class="stylebutton myCan">
         </td>
         <td>
          <input type="submit" id="issues5" onclick="submitIssues(this)" name="status" class="stylebutton myCan" value="Issue Found">
        </td>
        <td>
          <input type="submit" id="NA5" name="status" onclick="submitNA(this)" value="N/A" class="stylebutton myCan" style="width:100px;">
        </td>
      </tr>
      <tr>
        <th scope="row">
          MAC
        </th>
        <td>
         <input type="button" id="done6" onclick="submitButtonStyle(this)"  name="status" value="Done-No Issues" class="stylebutton myCan">
       </td>
       <td>
        <input type="submit" id="issues6" onclick="submitIssues(this)" name="status" class="stylebutton myCan" value="Issue Found">
      </td>
      <td>
        <input type="submit" id="NA6" name="status" onclick="submitNA(this)" value="N/A" class="stylebutton myCan" style="width:100px;">
      </td>
    </tr>
    <tr>
      <th scope="row">
        e-Office
      </th>
      <td>
       <input type="button" id="done7" onclick="submitButtonStyle(this)"  name="status" value="Done-No Issues" class="stylebutton myCan">
     </td>
     <td>
      <input type="submit" id="issues7" onclick="submitIssues(this)" name="status" class="stylebutton myCan" value="Issue Found">
    </td>
    <td>
      <input type="submit" id="NA7" name="status" onclick="submitNA(this)" value="N/A" class="stylebutton myCan" style="width:100px;">
    </td>
  </tr>
  <tr>
    <th scope="row">
      Intranet
    </th>
    <td>
     <input type="button" id="done8" onclick="submitButtonStyle(this)"  name="status" value="Done-No Issues" class="stylebutton myCan">
   </td>
   <td>
    <input type="submit" id="issues8" onclick="submitIssues(this)" name="status" class="stylebutton myCan" value="Issue Found">
  </td>
  <td>
    <input type="submit" id="NA8" name="status" onclick="submitNA(this)" value="N/A" class="stylebutton myCan" style="width:100px;">
  </td>
</tr>
<tr>
  <th scope="row">
    Public Website(www.wcf.go.tz)
  </th>
  <td>
   <input type="button" id="done9" onclick="submitButtonStyle(this)"  name="status" value="Done-No Issues" class="stylebutton myCan">
 </td>
 <td>
  <input type="submit" id="issues9" onclick="submitIssues(this)" name="status" class="stylebutton myCan" value="Issue Found">
</td>
<td>
  <input type="submit" id="NA9" name="status" onclick="submitNA(this)" value="N/A" class="stylebutton myCan" style="width:100px;">
</td>
</tr>
<tr>
  <th scope="row">
    WCFePG
  </th>
  <td>
   <input type="button" id="done10" onclick="submitButtonStyle(this)"  name="status" value="Done-No Issues" class="stylebutton myCan">
 </td>
 <td>
  <input type="submit" id="issues10" onclick="submitIssues(this)" name="status" class="stylebutton myCan" value="Issue Found">
</td>
<td>
  <input type="submit" id="NA10" name="status" onclick="submitNA(this)" value="N/A" class="stylebutton myCan" style="width:100px;">
</td>
</tr>
<tr>
  <th scope="row">
    Abacus/HRS
  </th>
  <td>
    <input type="button" id="done11" onclick="submitButtonStyle(this)"  name="status" value="Done-No Issues" class="stylebutton myCan">
  </td>
  <td>
    <input type="submit" id="issues11" onclick="submitIssues(this)" name="status" class="stylebutton myCan" value="Issue Found">
  </td>
  <td>
    <input type="submit" id="NA11" name="status" onclick="submitNA(this)" value="N/A" class="stylebutton myCan" style="width:100px;">
  </td>
</tr>
<tr>
  <th scope="row">
    ERP
  </th>
  <td>
   <input type="button" id="done12" onclick="submitButtonStyle(this)"  name="status" value="Done-No Issues" class="stylebutton myCan">
 </td>
 <td>
  <input type="submit" id="issues12" onclick="submitIssues(this)" name="status" class="stylebutton myCan" value="Issue Found">
</td>
<td>
  <input type="submit" id="NA12" name="status" onclick="submitNA(this)" value="N/A" class="stylebutton myCan" style="width:100px;">
</td>
</tr>
<tr>
  <th scope="row">
    Time and Attendance System
  </th>
  <td>
   <input type="button" id="done13" onclick="submitButtonStyle(this)"  name="status" value="Done-No Issues" class="stylebutton myCan">
 </td>
 <td>
  <input type="submit" id="issues13" onclick="submitIssues(this)" name="status" class="stylebutton myCan" value="Issue Found">
</td>
<td>
  <input type="submit" id="NA13" name="status" onclick="submitNA(this)" value="N/A" class="stylebutton myCan" style="width:100px;">
</td>
</tr>
</tbody>
</table>
</div>
</div>
</div>

<div class="col-xl-5">
  <div class="card shadow">
    <div class="card-header border-0">
      <div class="row align-items-center">
        <div class="col">
          <h3 class="mb-0">General Condition of Server Room(HQ)</h3>
        </div>
      </div>
    </div>
    <div class="table-responsive">
      <!-- Projects table -->
      <table class="table align-items-center table-flush">
        <thead class="thead-light">
          <tr>
            <th scope="col">DEVICE NAME</th>
            <th scope="col">OK</th>
            <th scope="col">NOT OK</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th scope="row">
              Air Contitioning(AC)
            </th>
            <td>
             <input type="button" id="done1" onclick="submitButtonStyle(this)"  name="status" value="Done-No Issues" class="stylebutton myCan">
           </td>
           <td>
            <input type="submit" id="issues1" onclick="submitIssues(this)" name="status" class="stylebutton myCan" value="Issue Found">
          </td>

        </tr>
        <tr>
          <th scope="row">
            Server Room UPs
          </th>
          <td>
           <input type="button" id="done1" onclick="submitButtonStyle(this)"  name="status" value="Done-No Issues" class="stylebutton myCan">
         </td>
         <td>
          <input type="submit" id="issues1" onclick="submitIssues(this)" name="status" class="stylebutton myCan" value="Issue Found">
        </td>
      </tr>
    </tbody>
  </table>
</div>
</div>
<br/>
<div class="card shadow">
  <div class="card-header border-0">
    <div class="row align-items-center">
      <div class="col">
        <h3 class="mb-0">General Condition of Server Room(DODOMA)</h3>
      </div>
    </div>
  </div>
  <table class="table align-items-center table-flush">
    <thead class="thead-light">
      <tr>
        <th scope="col">DEVICE NAME</th>
        <th scope="col">OK</th>
        <th scope="col">NOT OK</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <th scope="row">
          Air Contitioning(AC)
        </th>
        <td>
         <input type="button" id="done1" onclick="submitButtonStyle(this)"  name="status" value="Done-No Issues" class="stylebutton myCan">
       </td>
       <td>
        <input type="submit" id="issues1" onclick="submitIssues(this)" name="status" class="stylebutton myCan" value="Issue Found">
      </td>

    </tr>
    <tr>
      <th scope="row">
        Server Room UPs
      </th>
      <td>
       <input type="button" id="done1" onclick="submitButtonStyle(this)"  name="status" value="Done-No Issues" class="stylebutton myCan">
     </td>
     <td>
      <input type="submit" id="issues1" onclick="submitIssues(this)" name="status" class="stylebutton myCan" value="Issue Found">
    </td>
  </tr>
</tbody>
</table>
</div>
<br/>
<div class="card shadow">
  <div class="card-header border-0">
    <div class="row align-items-center">
      <div class="col">
        <h3 class="mb-0">Comments</h3>
      </div>
    </div>
  </div>
  {{-- <textarea id="w3review" name="w3review" rows="4" cols="50"> --}}
   {{--  <textarea  name="comment" value="" id="comment" placeholder="comments" cols="70"> </textarea> 
    <input type="checkbox" checked data-toggle="toggle" data-on="Ready" data-off="Not Ready" data-onstyle="success" data-offstyle="danger"> --}}
    <br/>
    <label>
      <h3 class="mb-0">Inspected By: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    <input type="text" name="inspected_by" id="inspected_by" value="{{auth()->user()->name}}" cols="50"></h3>
    </label>
    <label>
      <h3 class="mb-0">Inspecter Signature: &nbsp;&nbsp;<input type="text" name="signature" id="signature" value="signature" placeholder="Inspecter signature"></h3>  
    </label>
  </div>
</form>
</div>
</div>
</div>
</div>
</div>
</div>

@include('layouts.footers.auth')
<script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.min.js"></script>
<script src="{{ asset('argon') }}/vendor/chart.js/dist/Chart.extension.js"></script> 
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

@push('js')
<script type="text/javascript">
  function submitButtonStyle(_this) {
    _this.style.backgroundColor = "green";
  }
  function submitIssues(_this) {
    _this.style.backgroundColor = "red";
  }
  function submitNA(_this) {
    _this.style.backgroundColor = "yellow";
  }

  $(document).ready(function(){
    $(".myCan").click(function(e){
      e.preventDefault();
      var status;
      // alert($(this).attr('id'));
      $(this).attr('id')
      // this ($("#done").val() === "" ) {
      //   status = "Done No Issues";

      //   alert("Done No Issues")
      // }else if($("#issues").val() === "" ){
      //   status = "Issues Found";
      //   alert("Issues Found")     
      // }else{
      //   alert("None") 
      //   status = "N/A";
      // }

      comment = $("#comment").val();
      inspected_by = $("#inspected_by").val();
      signature =$("#signature").val();
      $.ajax({
        type:"POST",
        data:{"status":status, "comment":comment,"inspected_by":inspected_by,"signature":signature,"_token":"{{csrf_token()}}"},
        url:"{{URL::to('Health/daily')}}",
        success:function(data){
          console.log('karibu');
          $("#success").html(data);
          console.log('umefika');
          $("#myform")[0].reset();
          console.log('umefika');
        }
      });
    });
  });

//starting of saving data on database by url

</script>
@endpush
@endsection

{{--
  var status;
      if ("#done") {
        status = "Done No Issues";
      } else if ("#issues") {
        status = "Issues Found";
      } else {
        status = "N/A";
      }


       

 --}}