@extends('layouts.app')
@push('css')
<link type="text/css" href="{{ asset('argon') }}/vendor/momentjs/tempusdominus-bootstrap-4.min.css" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('argon') }}/vendor/fullcalendar/dist/fullcalendar.min.css">
<link rel="stylesheet" href="{{ asset('argon') }}/css/sweetalerts/sweetalerts.css"> 
<script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
{{-- <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script> --}}
<style type="text/css">
  .swal-text {
    text-align: left;
  }
</style>
@endpush


@section('content') 

@include('layouts.headers.daily')

<div class="container-fluid mt--3 on-loading">
  <div class="card bg-secondary shadow">
    <div class="card-header bg-white border-0">
      <div class="pt-3 card-body">
        <div class="col-xl-12 order-xl-1">
          <div class="card shadow">
            <form id="form-data" enctype="multipart/form-data">
              @csrf
              <div class="card-header border-0">
                <div class="row align-items-center">
                  <div class="col">
                    <h3 class="mb-0">AVAILABILITY OF SERVICE </h3>
                  </div>
                </div>
              </div>
              <br>
              <div class="first-page">
                <div class="card shadow">
                  <div class="card-header border-0">
                    <div class="row align-items-center">
                      <div class="col">
                        <h3 class="mb-0">General Condition of Server Room(HQ)</h3>
                      </div>
                    </div>
                  </div>
                  <div class="table-responsive">
                    <!-- Projects table -->
                    <table class="table align-items-center table-flush">
                      <thead class="thead-light">
                        <tr>
                          <th scope="col">1</th>
                          <th scope="col">DEVICE NAME</th>
                          <th scope="col">OK</th>
                          <th scope="col">NOT OK</th>
                          <th scope="col">COMMENTS</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <th scope="row">
                            1.1
                          </th>
                          <th scope="row">
                            Air Contitioning(AC)
                          </th>
                          <td>
                           <input type="button" id="done1" onclick="submitButtonStyle(this)"  name="status" value="Done-No Issues" class="stylebutton myCan">
                         </td>
                         <td>
                          <input type="submit" id="issues1" onclick="submitIssues(this)" name="status" class="stylebutton myCan" value="Issue Found">
                        </td>
                        <td>
                         <textarea type="submit" id="issues1" onclick="submitIssues(this)" name="status" class="stylebutton myCan" value="Issue Found" placeholder="comment if any"></textarea>
                       </td>
                     </tr>
                     <tr>
                      <th scope="row">
                       1.2
                     </th>
                     <th scope="row">
                      Server Room UPs
                    </th>

                    <td>
                     <input type="button" id="done1" onclick="submitButtonStyle(this)"  name="status" value="Done-No Issues" class="stylebutton myCan">
                   </td>
                   <td>
                    <input type="submit" id="issues1" onclick="submitIssues(this)" name="status" class="stylebutton myCan" value="Issue Found">
                  </td>
                  <td>
                   <textarea type="submit" id="issues1" onclick="submitIssues(this)" name="status" class="stylebutton myCan" value="Issue Found" placeholder="comment if any"></textarea>
                 </td>
               </tr>
             </tbody>
           </table>
         </div>
       </div>
       <br/>
       <div class="card shadow">
        <div class="card-header border-0">
          <div class="row align-items-center">
            <div class="col">
              <h3 class="mb-0">General Condition of Server Room(DODOMA)</h3>
            </div>
          </div>
        </div>
        <table class="table align-items-center table-flush">
          <thead class="thead-light">
            <tr>
              <th scope="col">1</th>
              <th scope="col">DEVICE NAME</th>
              <th scope="col">OK</th>
              <th scope="col">NOT OK</th>
              <th scope="col">COMMENTS</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope="row">
                1.3
              </th>
              <th scope="row">
                Air Contitioning(AC)
              </th>
              <td>
               <input type="button" id="done1" onclick="submitButtonStyle(this)"  name="status" value="Done-No Issues" class="stylebutton myCan">
             </td>
             <td>
              <input type="submit" id="issues1" onclick="submitIssues(this)" name="status" class="stylebutton myCan" value="Issue Found">
            </td>
            <td>
              <textarea type="submit" id="issues1" onclick="submitIssues(this)" name="status" class="stylebutton myCan" value="Issue Found" placeholder="comment if any"></textarea>
            </td>

          </tr>
          <tr>
            <th scope="row">
             1.4
           </th>
           <th scope="row">
            Server Room UPs
          </th>
          <td>
           <input type="button" id="done1" onclick="submitButtonStyle(this)"  name="status" value="Done-No Issues" class="stylebutton myCan">
         </td>
         <td>
          <input type="submit" id="issues1" onclick="submitIssues(this)" name="status" class="stylebutton myCan" value="Issue Found">
        </td>
        <td>
         <textarea type="submit" id="issues1" onclick="submitIssues(this)" name="status" class="stylebutton myCan" value="Issue Found" placeholder="comment if any"></textarea>
       </td>
     </tr>
   </tbody>
 </table>
</div>                                 

<div class="btn" style="float:right;">
 <button class="btn mb-1 btn-success btn-icon  btn-sm" id="first-page-next">@lang("Next Page")</button>
</div>
</div>
{{--End of first page  --}}
<div class="second-page" style="display: none;">
  <div class="table-responsive">
    <!-- Projects table -->
    <table class="table align-items-center table-flush">
      <thead class="thead-light">
        <tr>
          <th scope="col">Service Name</th>
          <th scope="col">UP/Pass</th>
          <th scope="col">DOWN/FAIL</th>
          <th scope="col">N/A</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th scope="row">
            vCenter Server
          </th>
          <td>


           <input type="submit" id="done" onclick="submitButtonStyle(this)"  name="vCenter Server" value="Done-No Issues" class="stylebutton myCan">
         </td>
         <td>
          <input type="submit" id="issues" onclick="submitIssues(this)" name="vCenter Server"  class="stylebutton myCan" value="Issue Found">

        </td>
        <td>
          <input type="submit" id="NA" onclick="submitNA(this)" name="vCenter Server" value="N_A" class="stylebutton myCan" style="width:100px;">

        </td>
      </tr>
      <tr>
        <th scope="row">
          Internet
        </th>
        <td>
         <input type="button" id="done1" onclick="submitButtonStyle(this)"  name="status" value="Done-No Issues" class="stylebutton myCan">
       </td>
       <td>
        <input type="submit" id="issues1" onclick="submitIssues(this)" name="status" class="stylebutton myCan" value="Issue Found">
      </td>
      <td>
        <input type="submit" id="NA1" name="status" onclick="submitNA(this)" value="N/A" class="stylebutton myCan" style="width:100px;">
      </td>

    </tr>
    <tr>
      <th scope="row">
       DHCP and DNS
     </th>
     <td>
       <input type="button" id="done2" onclick="submitButtonStyle(this)"  name="status" value="Done-No Issues" class="stylebutton myCan">
     </td>
     <td>
      <input type="submit" id="issues2" onclick="submitIssues(this)" name="status" class="stylebutton myCan" value="Issue Found">
    </td>
    <td>
      <input type="submit" id="NA2" name="status" onclick="submitNA(this)" value="N/A" class="stylebutton myCan" style="width:100px;">
    </td>
  </tr>
  <tr>
    <th scope="row">
      document Share
    </th>
    <td>
     <input type="button" id="done3" onclick="submitButtonStyle(this)"  name="status" value="Done-No Issues" class="stylebutton myCan">
   </td>
   <td>
    <input type="submit" id="issues3" onclick="submitIssues(this)" name="status" class="stylebutton myCan" value="Issue Found">
  </td>
  <td>
    <input type="submit" id="NA3" name="status" onclick="submitNA(this)" value="N/A" class="stylebutton myCan" style="width:100px;">
  </td>
</tr>
<tr>
  <th scope="row">
    Send and Receive Internal Email
  </th>
  <td>
   <input type="button" id="done4" onclick="submitButtonStyle(this)"  name="status" value="Done-No Issues" class="stylebutton myCan">
 </td>
 <td>
  <input type="submit" id="issues4" onclick="submitIssues(this)" name="status" class="stylebutton myCan" value="Issue Found">
</td>
<td>
  <input type="submit" id="NA4" name="status" onclick="submitNA(this)" value="N/A" class="stylebutton myCan" style="width:100px;">
</td>
</tr>
<tr>
  <th scope="row">
    Send and Receive External Email
  </th>
  <td>
   <input type="button" id="done5" onclick="submitButtonStyle(this)"  name="status" value="Done-No Issues" class="stylebutton myCan">
 </td>
 <td>
  <input type="submit" id="issues5" onclick="submitIssues(this)" name="status" class="stylebutton myCan" value="Issue Found">
</td>
<td>
  <input type="submit" id="NA5" name="status" onclick="submitNA(this)" value="N/A" class="stylebutton myCan" style="width:100px;">
</td>
</tr>
<tr>
  <th scope="row">
    MAC
  </th>
  <td>
   <input type="button" id="done6" onclick="submitButtonStyle(this)"  name="status" value="Done-No Issues" class="stylebutton myCan">
 </td>
 <td>
  <input type="submit" id="issues6" onclick="submitIssues(this)" name="status" class="stylebutton myCan" value="Issue Found">
</td>
<td>
  <input type="submit" id="NA6" name="status" onclick="submitNA(this)" value="N/A" class="stylebutton myCan" style="width:100px;">
</td>
</tr>
<tr>
  <th scope="row">
    e-Office
  </th>
  <td>
   <input type="button" id="done7" onclick="submitButtonStyle(this)"  name="status" value="Done-No Issues" class="stylebutton myCan">
 </td>
 <td>
  <input type="submit" id="issues7" onclick="submitIssues(this)" name="status" class="stylebutton myCan" value="Issue Found">
</td>
<td>
  <input type="submit" id="NA7" name="status" onclick="submitNA(this)" value="N/A" class="stylebutton myCan" style="width:100px;">
</td>
</tr>
<tr>
  <th scope="row">
    Intranet
  </th>
  <td>
   <input type="button" id="done8" onclick="submitButtonStyle(this)"  name="status" value="Done-No Issues" class="stylebutton myCan">
 </td>
 <td>
  <input type="submit" id="issues8" onclick="submitIssues(this)" name="status" class="stylebutton myCan" value="Issue Found">
</td>
<td>
  <input type="submit" id="NA8" name="status" onclick="submitNA(this)" value="N/A" class="stylebutton myCan" style="width:100px;">
</td>
</tr>
<tr>
  <th scope="row">
    Public Website(www.wcf.go.tz)
  </th>
  <td>
   <input type="button" id="done9" onclick="submitButtonStyle(this)"  name="status" value="Done-No Issues" class="stylebutton myCan">
 </td>
 <td>
  <input type="submit" id="issues9" onclick="submitIssues(this)" name="status" class="stylebutton myCan" value="Issue Found">
</td>
<td>
  <input type="submit" id="NA9" name="status" onclick="submitNA(this)" value="N/A" class="stylebutton myCan" style="width:100px;">
</td>
</tr>
<tr>
  <th scope="row">
    WCFePG
  </th>
  <td>
   <input type="button" id="done10" onclick="submitButtonStyle(this)"  name="status" value="Done-No Issues" class="stylebutton myCan">
 </td>
 <td>
  <input type="submit" id="issues10" onclick="submitIssues(this)" name="status" class="stylebutton myCan" value="Issue Found">
</td>
<td>
  <input type="submit" id="NA10" name="status" onclick="submitNA(this)" value="N/A" class="stylebutton myCan" style="width:100px;">
</td>
</tr>
<tr>
  <th scope="row">
    Abacus/HRS
  </th>
  <td>
    <input type="button" id="done11" onclick="submitButtonStyle(this)"  name="status" value="Done-No Issues" class="stylebutton myCan">
  </td>
  <td>
    <input type="submit" id="issues11" onclick="submitIssues(this)" name="status" class="stylebutton myCan" value="Issue Found">
  </td>
  <td>
    <input type="submit" id="NA11" name="status" onclick="submitNA(this)" value="N/A" class="stylebutton myCan" style="width:100px;">
  </td>
</tr>
<tr>
  <th scope="row">
    ERP
  </th>
  <td>
   <input type="button" id="done12" onclick="submitButtonStyle(this)"  name="status" value="Done-No Issues" class="stylebutton myCan">
 </td>
 <td>
  <input type="submit" id="issues12" onclick="submitIssues(this)" name="status" class="stylebutton myCan" value="Issue Found">
</td>
<td>
  <input type="submit" id="NA12" name="status" onclick="submitNA(this)" value="N/A" class="stylebutton myCan" style="width:100px;">
</td>
</tr>
<tr>
  <th scope="row">
    Time and Attendance System
  </th>
  <td>
   <input type="button" id="done13" onclick="submitButtonStyle(this)"  name="status" value="Done-No Issues" class="stylebutton myCan">
 </td>
 <td>
  <input type="submit" id="issues13" onclick="submitIssues(this)" name="status" class="stylebutton myCan" value="Issue Found">
</td>
<td>
  <input type="submit" id="NA13" name="status" onclick="submitNA(this)" value="N/A" class="stylebutton myCan" style="width:100px;">
</td>
</tr>
</tbody>
</table>
</div>

<div class="btn" style="float:right;">
 <button class="btn  btn-success btn-icon  btn-sm " id="second-page-prev">@lang("Previous Page")</button>&nbsp;
 <button class="btn  btn-success btn-icon  btn-sm" id="second-page-next">@lang("Next Page")</button>
</div>
</div> 
{{-- End of second page --}}

{{--start of another page  --}}
<div class="third-page" style="display: none;">
 <div class="row">
  <div class="col-md-6">
    <div class="form-group mb-4">
      <label  for="exampleFormControlSelect1">Financial /Year </label>
      <select class="form-control" name="fin_year" id="fin_year" >
        <option selected disabled hidden>Financial /Year ..</option>
        <option>2015/2016</option>
        <option>2016/2017</option>
        <option>2017/2018</option>
        <option>2018/2019</option>
        <option>2019/2020</option>
      </select>
    </div>
  </div>
  <div class="col-md-6">
   <div class="form-group reminder-class">
     <label class="form-control-label" for="end_date">{{ __('Set Completion Reminder Date') }}</label>
     <div class="input-group date" id="datetimepicker6" data-target-input="nearest">
      <input type="text"  name="reminder_date" class="form-control datetimepicker-input" id="reminder-date" data-target="#datetimepicker6" />
      <div class="input-group-append" data-target="#datetimepicker6" data-toggle="datetimepicker">
        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
      </div>
    </div>
  </div>
</div>
</div>
<div class="row notify" style="display: none;">
 <div class="col-md-6 notifie-users">
  <label for="notifie_users">Select Users to include in reminder</label>
  <select multiple="true" class="form-control" name="notifie_users[]" style="height: 100px" id="users_ids">
  </select>
</div>
<div class="col-md-6 notifie-suppliers" style="margin-top: 60px;">
  <label for="notifie_supplier" id="notify-supplier" style="display: none;"><b>Check to notify supplier </b></label>
  <label for="notifie_supplier" id="notify-beneficiary" style="display: none;"><b>Check to notify beneficiary </b></label>
  <input type="checkbox" name="notifie_supplier" id="supplier-notify" value="{{0}}" >
</div>
</div>
<div class="row">
 <div class="col-md-6">
  <div class="form-group">
   <label id="supplier-name">Name of Supplier/Contractor</label>
   <label id="beneficiary-name" style="display: none;">Name of Beneficiary</label>
   <div class="form-group">
     <input type="text" name="contract_supplier_name" id="supplier-id" class="form-control" placeholder="type supplier name....">
     <input type="text" name="contract_supplier_name" id="beneficiary-id" class="form-control" placeholder="type beneficiary name...." style="display: none;">
     <input type="hidden" name="contract_supplier_id" id="supp-tin">
     <div id="suppliers-only">
       <table class="table table-bordered"  id="search-supp">
        <tbody>
          {{-- @foreach($suppliers->employer as $e) --}}
          <tr style="display: none">
            <td style="display: none" class="tin-number">#</td>
            <td class="supp-name">#</td>
          </tr>
          {{-- @endforeach --}}
        </tbody>
      </table>
    </div>
    <div id="beneficiaries-only">
      <table class="table table-bordered"  id="search-ben">
        <tbody>
          {{-- @foreach($users as $u) --}}
          <tr style="display: none">
            <td style="display: none" class="ben-id">#</td>
            <td class="ben-name">{{-- {{$u->firstname.' '.$u->middlename.' '.$u->lastname}} --}}</td>
          </tr>
          {{-- @endforeach --}}
        </tbody>
      </table>
    </div>
  </div>
</div>
</div>
</div>
<div class="btn" style="float:right;">
 <button class="btn  btn-success btn-icon  btn-sm " id="third-page-prev">@lang("Previous Page")</button>&nbsp;
 <button class="btn  btn-success btn-icon  btn-sm " id="third-page-next">@lang("Next Page")</button> 
</div> 
</div>

<div class="fourth-page" style="display: none;"> 
  <div class="btn" style="float:right;">
   <button class="btn  btn-success btn-icon  btn-sm " id="last-page-prev">@lang("Previous Page")</button>&nbsp;
   <button class="btn  btn-info btn-icon  btn-sm " type="submit" id="submit-page"><i class="fas fa-location-arrow" style="font-size: 18px;">@lang("submit")</i></button> 
 </div> 
</div>
</form>
</div>
</div>
</div>
</div>
</div>
</div>




@include('layouts.footers.auth')
@endsection
@push('js')
{{-- <script type="text/javascript" src="{{ asset('argon') }}/css/sweetalerts/sweetalerts.js"></script>
<script type="text/javascript" src="{{ asset('argon') }}/vendor/momentjs/moment-with-locales.js"></script>
<script type="text/javascript" src="{{ asset('argon') }}/vendor/momentjs/tempusdominus-bootstrap-4.min.js"></script>
<script type="text/javascript" src="{{ asset('argon') }}/plugins/maskmoney/js/maskmoney.min.js"></script> --}}
<script type="text/javascript">
  $('.on-loading').hide();

  $(document).ready(function(){
    $('.on-loading').show();
     // $('.first-page').hide();
     $('.second-page').hide();
     $('.third-page').hide();
     $('.fourth-page').hide();
     $('#third-page-next').hide();
     $('.supplier-details').hide();
     $('.beneficiary-details').hide();
     $('#proc-type').hide();

     $('#search-ben tbody').on('click', 'tr', function (e) {
          // var data = table.row( this ).data();
           var $item = $(this).closest("tr")   // Finds the closest row <tr> 
                       .find(".ben-name")     // Gets a descendent with class="ben-name"
                       .text(); 

          var $id = $(this).closest("tr")   // Finds the closest row <tr> 
                       .find(".ben-id")     // Gets a descendent with class="ben-id"
                       .text(); 
                       $('#beneficiary-id').val($item)
                       $('#supp-tin').val($id);
                       $('#search-ben').fadeOut();

                       var benId = $id;
          // var urlCheck = "{{url('contracts/check_beneficiary')}}/"+benId;
          // var urls = "{{url('contracts/supplier_details')}}";
          var token = "{{csrf_token()}}";

          $.ajax({
            url: urlCheck,
            method:"POST",
            data: {
              '_token' : token,
              'tin' : benId,
            },
            success:function(data)
            {

            }
          });


          $('#third-page-next').show();
        }
        )



     function search_table(value){
      $('#search-supp tr').each(function(){
        $(this).each(function(){
          if ($(this).text().toLowerCase().indexOf(value.toLowerCase()) >= 0) {
              // found = 'true';
              $(this).show()
            }else{
              $(this).hide()
            }
          })
      })
    }

    $('#beneficiary-id').keyup(function(e){
      var values = $(this).val();
      if (values.length > 2) {
        $('#third-page-next').hide();
        $('#search-ben').show()
        search_user_table($(this).val())
      }else{
        $('#search-ben').hide()
      }
    });

    function search_user_table(value){
      $('#search-ben tr').each(function(){
        $(this).each(function(){
          if ($(this).text().toLowerCase().indexOf(value.toLowerCase()) >= 0) {
              // found = 'true';
              $(this).show()
            }else{
              $(this).hide()
            }
          })
      })
    }  


    $('#second-page-next').click(function(e){
      e.preventDefault();
        //get unit id
        var cat = $('#category-id option:selected').val();
        //check if inputs filled in second page
        var errors = [];
        var start_date = $('#date1').val();
        var end_date = $('#date3').val();
        var inspecter = $('#inspecter-id').val();
        var verifier = $('#inspecter-id').val();
        var start = new Date($('#date1').val())


        var start_dateerror = 'Start date field is required \n';
        var delivery_dateerror = 'Delivery date field is required \n';
        var end_dateerror = 'End date field is required \n';
        var warant_start_dateerror = 'Warant start date field is required \n';
        var warant_end_dateerror = 'Warant end date field is required \n';
        var perfomance_bond_starterror = 'Perfomance bond start field is required \n';
        var perfomance_bond_enderror = 'Perfomance bond end field is required \n';
        var managererror = 'Manager field is required \n';
        var datebalancerror = 'Start date must be less than end date! \n';
        var deliverbalancerror = 'Delivery date must be in between start date and end date! \n';
        

        if (inspecter == null) {
          errors[0] =managererror;
        }
        if (start_date.length == 0) {
          errors[1] = start_dateerror;
        }
        if ((cat == 3) || (cat == 6)) {
          if (delivery_date.length == 0) {
            errors[2] = delivery_dateerror;
          }
        }

        if (end_date.length == 0) {
          errors[3] = end_dateerror;
        }
        if (start_date.length > 0 && end_date.length > 0) {
          if (start.getTime() >= end.getTime()) {
            errors[4] = datebalancerror;
          }
        }
        if (delivery_date.length > 0) {
          if (start.getTime() < deliver.getTime() && deliver.getTime() < end.getTime()) {

          }else{
            errors[5] = deliverbalancerror;
          }
        }

        var errorBag = [];
        for(var i=0; i < errors.length; i++)
        {
          if (errors[i] != null) {
            errorBag[i] = errors[i]
          }

        }

        var html = "";
        var count = 1;

        $.each(errorBag, function(key, d)
        {
          if (d == undefined) {
           count--;
         }else{
          html += count+". "+d;
        }
        count ++;
      });

        str=errorBag.join("");
        if (str.length > 0) {
          swal({
            title: "Warning",
            text: html,
            type: "warning",
            confirmButtonClass: 'btn btn-primary',
            confirmButtonText: 'OK!'
          })


          $('.second-page').show();
          $('.third-page').hide();
          return false;
        }
        else{

          $('.second-page').hide();
          $('.third-page').show();

        }
      });


    $('#unit-id').change(function(){
      var unit = $(this).val();
      //fetch user dept
     // var url = "{{url('contracts/user_dept_head')}}/"+unit;
     $.ajax({
       type: 'GET',
       url: url,
       success:function(response){
        var getresponse = '';
        for(var i in response){
          getresponse +=
          '<option value="'+response[i].id+'">'+response[i].name+'</option>';
        }
        $('#manager-id').empty();
        $('#manager-id').append(getresponse); 
          // $('.notify').show();
        }
      });
   });

    function unitId(unit){
      return unit;
    }

    $('#first-page-next').click(function(e){
      e.preventDefault();

      var catId = $('#category-id option:selected').val();
      var loan = $('#loan-category option:selected').val();
      var hloan = $('#house-loan option:selected').val();


        //check if inputs filled in first page
        var errors = [];
        var category = $('#category-id').val();
        var unit = $('#unit-id').val();
        var description = $('#description').val();
        var payterm = $('#payment-term').val();
        var amtId = $('#amount-id').val();
        // amtId.replace(/,/g, '');
        var num_array = amtId.split(',');
        amtId = num_array.join('');


        var caterror = 'Category field is required \n';
        var uniterror = 'Unit field is required \n';
        var descerror = 'Description field is required \n';
        var payerror = 'Payment field is required \n';
        var contNoerror = 'Contract number field is required \n';
        var pferror = 'Payment frequence field is required \n';
        var aerror = 'Amount field is required \n';
        var exceedError = 'The summation of instalments is not equal to total amount \n';
        var procError = 'Procurement type field is required! \n';
        var loanerror = 'Loan Category field is required \n';
        var hloanerror = 'House Loan field is required \n';

        if (category == null) {
          errors[0] =caterror;
        }
        if (unit == null) {
          errors[1] = uniterror;
        }
        if (description.length == 0) {
          errors[2] = descerror;
        }
        if (catId != 5) {
          if (payterm == null) {
            errors[3] = payerror;
          }else{
            if (payterm == 2) {
              var pf = $('#frequence').val()
              if (pf.length > 0 ) {
                // check if frequency data exists
                
                var div = $('#form-data').find('.milestone-row');
                if (div[0] == undefined) {

                 errors[6] = 'Click generate field and add the required fields!';
               }else{

                var amt = $('#form-data').find('.amount-milestone');
                var date = $('#form-data').find('.payment-date');

                var perrors = []
                var derrors = []

                var i = 0;
                var phase = 1;
                var total = 0;
                var $prevPhase = 1;
                while (i < pf)
                {
                  var pay = $('#form-data').find('.amount-milestone')[i].value.length;
                  var datee = $('#form-data').find('.payment-date')[i].value.length;
                  var pdate = $('#form-data').find('.payment-date')[i].value;
                  if (pay == 0 && datee == 0) {
                    perrors[i] = 'Milestone payment'+ phase +' field is required! and Due date'+phase+' field is required! \n';
                  }
                  else if (pay == 0 && datee > 0) {
                    perrors[i] = 'Milestone payment'+ phase +' field is required! \n';
                  }
                  else if (pay > 0 && datee == 0) {
                    perrors[i] = 'Due date'+ phase +' field is required! \n';
                  }
                  var payment = $('#form-data').find('.amount-milestone')[i].value;
                  total += parseFloat(payment);

                  i++
                  phase++
                }


                errors[6] = perrors;
                      // errors[13] = derrors;

                      //summation errors
                      var amtId = $('#amount-id').val();
                      amtId.replace(/,/g, '');
                      var num_array = amtId.split(',');
                      amtId = num_array.join('');
                      if ((total > (parseFloat(amtId) + 0.5)) || (total < (parseFloat(amtId) - 0.5))) {
                        errors[8] = exceedError;
                      }

                      
                      var selected = $('#form-data').find('#bank-g').val();
                      if (selected == 1) {
                        var exists = $('#form-data').find('#bank-file').get(0).files.length;
                        if (exists > 0) {
                         var fileInput = $('#form-data').find('#bank-file').get(0).files[0].name;
                         var str = fileInput;
                         var [filename, extension] = str.split('.')
                         .reduce((acc, val, i, arr) => (i == arr.length - 1) 
                          ? [acc[0].substring(1), val] 
                          : [[acc[0], val].join('.')], [])

                         if (extension !== "pdf") {
                          errors[9] = "Invalid file extension.Supported format extension is .pdf! \n";
                        }
                      }else{
                        errors[9] = "Bank guarantee attachment is required! \n";
                      }
                    }

                  }

                }else{
                  errors[5] = pferror;
                }
              }
            }
          }

          if (contractNo.length == 0) {
            errors[4] = contNoerror;
          }
          if (catId != 5) {
            if (amtId.length == 0) {
              errors[7] = aerror;
            }
          }

          
          if (catId == 1 ) {
            if (proc == null) {
              errors[10] = procError;
            }
          }else{
            setTimeout(() => { },60)
          }
          if (catId == 2) {
            if (isNaN(loan)) {
              errors[11] = loanerror;
            }else{
              if (loan == 2) {
                if (isNaN(hloan)) {
                  errors[12] = hloanerror;
                }
              }
            }
            
          } 

          var errorBag = [];
          for(var i=0; i < errors.length; i++)
          {
            if (errors[i] != null) {
              errorBag[i] = errors[i]
            }

          }

          var html = "";
          var count = 1;

          $.each(errorBag, function(key, d)
          {
            if (d == undefined) {
             count--;
           }else{
            html += count+". "+d;
          }
          count ++;
        });



          str=errorBag.join("");
        // return false;
        if (str.length > 0) {
          swal({
            title: "Warning",
            text: html,
            type: "warning",
            confirmButtonClass: 'btn btn-primary',
            confirmButtonText: 'OK!'
          })


          $('.first-page').show();
          $('.second-page').hide();
          return false;
        }else{
          $('.first-page').hide();
          $('.second-page').show();
        }


      });

$('#third-page-next').click(function(e){
  e.preventDefault();
  var catId = $('#category-id option:selected').val()

        //check if inputs filled in first page
        var daterror = '1. Finish date and Time must be greater than start date! \n';
        var $end_date = new Date($('#date3').val())
        var $reminder_date = new Date($('#reminder-date').val())

        if ($end_date.getTime() <= $reminder_date.getTime()) {
          swal({
            title: "Error",
            text: daterror,
            type: "error",
            confirmButtonClass: 'btn btn-primary',
            confirmButtonText: 'OK!'
          })
        }
        else{
          $('.third-page').hide();
          $('.fourth-page').show();
        }       
      });
$('#second-page-prev').click(function(e){
  e.preventDefault();
  $('.second-page').hide();
  $('.first-page').show();
});
$('#third-page-prev').click(function(e){
  e.preventDefault();
  $('.third-page').hide();
  $('.second-page').show();
});
$('#last-page-prev').click(function(e){
  e.preventDefault();
  $('.fourth-page').hide();
  $('.third-page').show();
});


function N(num, places) 
{ return +(Math.round(num + "e+" + places)  + "e-" + places);
}

$('#form-data').submit(function(e){
  e.preventDefault();
  $('#submit-page').attr('disabled',true);
  var check = $('input[type=checkbox]').prop('checked');
  var catId = $('#category-id option:selected').val();
  if (check == true) {
    $('input[type=checkbox]').val(1);
  }
  var $form = $("#form-data");
  var $data = $($form).serializeArray();
  if (catId == 2) {
    $data.push({'name':'isbeneficiary', 'value': 1});
  }

  var catId = $('#category-id option:selected').val();
  var token = "{{csrf_token()}}";
        // var urls = "{{url('contracts/store')}}";
        $.ajax({
         url: urls,
         method:"POST",
         // data: $data,
         data:new FormData(this),
         dataType:'JSON',
         contentType: false,
         cache: false,
         processData: false,
         success:function(data)
         {
          // return false;
          var abc = data;
          var errorBag = []
          $.each(abc,function(i,a){
            // errorBag[i] = a;
          });
          

          if (data.errors) {
            var html = "";
            var count = 1;

            $.each(data.errors, function(key, d)
            {
              html += count+" ."+d+"\n";
              count ++;
            });

            swal({
              title: "Error",
              text: html,
              type: "error",
              confirmButtonClass: 'btn btn-primary',
              confirmButtonText: 'OK!'
            })
            $('#submit-page').attr('disabled',false);
          } 
         
         if (data.success) {
          swal({
            title: "Success",
            text: "Checklist Successful registered!",
            type: "success",
            confirmButtonClass: 'btn btn-primary',
            confirmButtonText: 'OK!'
          },function(){ location.href = "{{url('/days')}}"; })
        } 

        if (data.unknown) {
          swal({
            title: "Error",
            text: data.unknown,
            type: "error",
            confirmButtonClass: 'btn btn-primary',
            confirmButtonText: 'OK!'
          })
        } 
      }
    });

      });
$(function () {
  $('#datetimepicker1').datetimepicker({  


  });
});
$(function () {
  $('#datetimepicker2').datetimepicker({  


  });
});
$(function () {
  $('#datetimepicker3').datetimepicker({  


  });

$('#closing').on('click',function(){
  $('#myModal').hide();
});
$("#datetimepicker1").on("change.datetimepicker", function (e) {
  $(this).datetimepicker("hide")
})
$("#datetimepicker2").on("change.datetimepicker", function (e) {
  $(this).datetimepicker("hide")
})
$("#datetimepicker3").on("change.datetimepicker", function (e) {
  $(this).datetimepicker("hide")
})


$("#datetimepicker7").on("change.datetimepicker", function (e) {
  $(this).datetimepicker("hide")
})
$("#datetimepicker8").on("change.datetimepicker", function (e) {
  $(this).datetimepicker("hide")
})

$("#datetimepicker6").on("change.datetimepicker", function (e) {
 var input = $("#reminder-date").val();
 var catId = $('#category-id option:selected').val();
 if (input.length == 0) {
  $('.notify').hide();
}else{
  var catId = $('#category-id option:selected').val();
  var url = "{{url('contracts/users')}}";
  $.ajax({
   type: 'GET',
   url: url,
   success:function(response){
    var getresponse = '';
    for(var i in response){
      getresponse +=
      '<option value="'+response[i].id+'">'+response[i].name+'</option>';
    }
    $('#users_ids').empty();
    $('#users_ids').append(getresponse); 

    if (catId == 2) {
      $('#notify-supplier').hide();
      $('#notify-beneficiary').show();
    }else{
     $('#notify-supplier').show();
     $('#notify-beneficiary').hide();
   }

   $('.notify').show();
 }
});
}
$(this).datetimepicker("hide")
});

}); 
});


</script>
@endpush

