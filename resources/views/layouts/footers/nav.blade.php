<div class="row align-items-center justify-content-xl-between">
    <div class="col-xl-6">
        <div class="copyright text-center text-xl-left text-muted">
            &copy; {{ now()->year }} <a href="#" class="font-weight-bold ml-1" target="_blank">Wcf</a> &amp;
            <a href="#" class="font-weight-bold ml-1" target="_blank">Ict Team</a>
        </div>
    </div>
    <div class="col-xl-6">
        <ul class="nav nav-footer justify-content-center justify-content-xl-end">
            <li class="nav-item">
                <a href="https://www.wcf.go.tz" class="nav-link" target="_blank" style="color: #000fff">WCF Website</a>
            </li>
            <li class="nav-item">
                <a href="http://192.168.1.9/ims/public" class="nav-link" target="_blank" style="color: #000fff">IMS</a>
            </li>
            

        </ul>
    </div>
</div>