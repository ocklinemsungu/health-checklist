<div class="header  pb-5 pt-5 pt-md-6" style="background-color: #52AF81;">
{{-- #C6C4C8;"> --}}
    <div class="container-fluid">
        <div class="header-body">
            <div class="row justify-content-center">
                <div class="col-lg-5 col-md-6">
                    <h1 class="text-white">{{ __('Weakly Health Checklist') }}</h1>
                </div>
            </div>
            </div>
        </div>
    </div>