@extends('layouts.app')
@section('content') 
    <div class="container pb-4">
        <div class="row justify-content-center py-3">
            <a href="#">
                <img src="{{asset('/argon/img/brand/logo.png')}}" defer style="width:300px;height:80px;" alt="SAMILL">

            </a>
        </div>
        <div class="row justify-content-center pb-3">
                 <h1 class="text-center">ICT HEALTH CHECKLIST SYSTEM<h1>
        </div>
        <div class="row justify-content-center">
            {{-- {{bcrypt('987651')}} --}}
            <div class="col-lg-5 col-md-7">
                <div class="card bg-secondary shadow">                    
                    <div class="card-body px-lg-5 py-lg-5" style="border:1px solid rgba(50, 50, 93, .5); border-radius: 5px;">                        
                        <form role="form" method="POST" action="{{ route('login') }}">
                            @csrf
                            <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }} mb-3">
                                <div class="input-group input-group-alternative">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                                    </div>
                                    <input class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="{{ __('email') }}" type="email" name="email" value="{{ $errors->has('email') ? old('email') : '' }}" style="border:1px solid rgba(50, 151, 211, .25); padding-left: 5px;" required autofocus>
                                </div>
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" style="display: block;" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('password') ? ' has-danger' : '' }}">
                                <div class="input-group input-group-alternative">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fas fa-lock-open"></i></span>
                                    </div>
                                    <input class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="{{ __('Password') }}" type="password"  style="border:1px solid rgba(50, 151, 211, .25); padding-left: 5px;" required>
                                </div>
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" style="display: block;" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="custom-control custom-checkbox">
                                <input class="custom-control-input" name="remember" id="customCheckLogin" type="checkbox" style="outline: 1px solid rgb(50, 151, 211);" {{ old('remember') ? 'checked' : '' }}>
                                <label class="custom-control-label" for="customCheckLogin">
                                    <span class="text-muted">{{ __('Remember me') }}</span>
                                </label>
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn-primary my-4">{{ __('Sign in') }}</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-6">
                        @if (Route::has('password.request'))
                            <a href="{{ route('password.request') }}">
                                <small>{{ __('Forgot password?') }}</small>
                            </a>
                        @endif
                    </div>
                    <div class="col-6 text-right">
                        <a href="{{ route('register') }}">
                            <small>{{ __('Create new account') }}</small>
                        </a>
                    </div>
                </div> 
            </div>
        </div>
    </div>
@endsection




{{-- @extends('layouts.app', ['class' => 'bg-default'])

@section('content')
    <div class="header bg-gradient-primary py-7 py-lg-8">
        <div class="container">
            <div class="header-body text-center mt-7 mb-7">
                <div class="row justify-content-center">
                    <div class="col-lg-5 col-md-6">
                        <h1 class="text-white">{{ __('Welcome to Health Checklist ') }}</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="separator separator-bottom separator-skew zindex-100">
            <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">
                <polygon class="fill-default" points="2560 0 2560 100 0 100"></polygon>
            </svg>
        </div>
    </div>

    <div class="container mt--10 pb-5"></div>
@endsection --}}
