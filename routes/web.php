<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function () {
	Route::resource('user', 'UserController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);
});

Route::group(['prefix' => 'Health','as'=>'health.'],function(){

	Route::get('daily','DailyController@DailyDetail')->name('daily');
	Route::post('daily','DailyController@store')->name('daily');


              // Route::get()->name();

});
			//*****************************
			//starting of  compilation Weakly Checklist
Route::group(['prefix' => 'Health','as'=>'Weakly.'],function(){

	Route::get('weakly','WeaklyController@WeaklyDetail')->name('weakly');
	Route::get('create','WeaklyController@create')->name('create');
	Route::post('store', 'WeaklyController@store')->name('store');

});
			 //***************************

			 //starting of Completion of 
Route::group(['prefix' => 'Health','as'=>'Completion.'],function(){

	Route::get('completion','CompletionController@index')->name('completion');

});
Route::group(['prefix' => 'Health','as'=>'Report.'],function(){

	Route::get('achieves','ReportsController@index')->name('achieves');

});

Route::group(['prefix' => 'Health','as'=>'Report.'],function(){

	Route::get('overviews','ReportsController@viewDetail')->name('overview');

});
Route::group(['prefix' => 'Health','as'=>'health.'],function(){


});

