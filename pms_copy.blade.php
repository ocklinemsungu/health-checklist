  
public function datatableRevenueImplementation($fy_id,$quarter_id,$department_id,$unit_id=null)
{ 
  $action_revenue_implementation = ActionIncomeImplementations::select('action_incomes.unit','action_incomes.objective','action_incomes.target','action_incomes.annual_planned_activity','action_income_implementations.target_in_tzs','action_income_implementations.implemented_in_tzs','action_income_implementations.implementation_rate','action_income_implementations.reason_for_variance_or_remark','action_income_implementations.total_implementation_rate','action_income_implementations.variance_or_remark','action_income_implementations.id as id','financial_year_id.description as year')
  ->join('quarters','action_income_implementations.quarter_id','=','quarters.id')
  ->join('action_incomes','action_income_implementations.action_income_id','=','action_incomes.id')
  ->join('units','action_incomes.unit_id','=','units.id')
  ->where('quarter_id',$quarter_id)
  ->where('financial_year_id',$fy_id);

  if (!empty($unit_id)) {
  $action_revenue_implementation->where('unit_id',$unit_id);
} else {
$units = Unit::where('department_id',$department_id)->pluck('name');
$action_revenue_implementation->whereIn('unit',$units);
}
$action_revenue_implementation->orderBy('unit_id','DESC')->orderBy('action_income_implementations.id','ASC');
$can_update = $this->canUpdateRevenueImplementation($fy_id,$quarter_id,$unit_id);
return DataTables::of($action_revenue_implementation)
->editColumn("implemented_in_tzs", function($data) use($can_update) {
$implemented_in_tzs = !empty($data->implemented_in_tzs) ? number_format($data->implemented_in_tzs,2) : $data->implemented_in_tzs;
return $can_update ?  '<a href="" class="update_imple_tzs money" data-name="implemented_in_tzs" data-type="text" data-min="0" data-pk="'.$data->id.'" >'.$implemented_in_tzs.'</a>' : $implemented_in_tzs;   
})->editColumn("implementation_rate", function($data) use($can_update) {
return  !empty($data->implementation_rate) ? round($data->implementation_rate,2).'%' : $data->implementation_rate;
})->editColumn("reason_for_variance_or_remark", function($data) use($can_update) {
if($data->implementation_rate == 0 && $data->implementation_rate != null){
return '';  
}else{
return $can_update ?  '<a href="" class="update_reason_for_variance" data-name="reason_for_variance_or_remark" data-type="textarea" data-pk="'.$data->id.'" >'.$data->reason_for_variance_or_remark.'</a>' : $data->reason_for_variance_or_remark;
}
})->editColumn("total_implementation_rate", function($data) use($can_update) {
return  !empty($data->total_implementation_rate) ? round($data->total_implementation_rate,2).'%' : $data->total_implementation_rate;
})->editColumn("variance_or_remark", function($data) use($can_update) {
if($data->total_implementation_rate == 0 && $data->total_implementation_rate != null){
return '';  
}else{
return $can_update ?  '<a href="" class="update_variance_remark" data-name="variance_or_remark" data-type="textarea" data-pk="'.$data->id.'" >'.$data->variance_or_remark.'</a>' : $data->variance_or_remark;
}
})->editColumn("target_in_tzs", function($data){
return number_format($data->target_in_tzs,2);
})->rawColumns(['implemented_in_tzs','reason_for_variance_or_remark','variance_or_remark'])->make(true);
} 



<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\User;

class PermissionRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

      \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
      \DB::table('role_has_permissions')->truncate();
      \DB::table('model_has_permissions')->truncate();
      \DB::statement('SET FOREIGN_KEY_CHECKS=1;');

      app()['cache']->forget('spatie.permission.cache');


      $role_dg = Role::find(1);
      $role_director = Role::find(2);
      $role_hod = Role::find(3);
      $role_manager = Role::find(4);
      $role_officer = Role::find(5);
      $role_admin = Role::find(6);

      $prm = User::where('designation','PRM')->first();

      $dfpi = User::where('designation','DFPI')->first();


      $role_dg->givePermissionTo(['view tier one menu', 'upload tier one scorecard','approve tier two scorecard']);

      $role_director->givePermissionTo(['view tier two menu', 'upload tier two scorecard','submit perfomance review','approve tier two b scorecard','approve quater target result','update variance reason','submit department implementation plan','submit department revenue implementation plan','approve unit implementation plan','approve unit revenue implementation plan']);

      $role_hod->givePermissionTo(['view tier two menu', 'upload tier two scorecard','submit perfomance review','assign activities','approve activities weight','update quater target result','update variance reason','delegate permission','update implementation plan','updata revenue implementation plan','submit department implementation plan','submit department revenue implementation plan']);

      $role_manager->givePermissionTo(['view tier two b menu', 'upload tier two b scorecard','submit quater target result','assign activities','approve activities weight','update quater target result','delegate permission','update implementation plan','updata revenue implementation plan','submit unit implementation plan','submit unit revenue implementation plan']);

      $role_officer->givePermissionTo(['update activities weight']);

      $role_admin->givePermissionTo(['view tier one menu', 'upload tier one scorecard','assign computation formula']);

      if (count($prm)) {
        $prm->givePermissionTo(['consolidate perfomance review','upload actionplan','upload actionrevenue','view tier one menu','upload tier one scorecard','assign computation formula','approve department implementation plan','approve department revenue implementation plan']);  
      }

      if (count($dfpi)) {
       $dfpi->givePermissionTo(['approve consolidated perfomance review']);  
     }



     $this->command->info('Roles has been assigned permissions successfully');
   }


 }


 'action_income_implementations.financial_year_id as year'


 $(document).ajaxComplete(function() {

  $('.update_revenue_implementation').editable({
    step: 'any',
    params: function(params) {
      return params;
    },
    url: '{{route('actionplan.update_revenue_budget')}}',
    success: function (data, response) {
      toastr["success"]("Success! Total actual collection  saved", "Dear! {{Auth::user()->firstname}}");
    },
    error: function () {
      toastr["error"]("Request Failed to save Total actual collection! An error has occured", "Sorry! {{Auth::user()->firstname}}");
    }
  });
});





 if($user->can('submit cordinated actionplan')){
  if ($this->financial_year->id == $fy_id) {
    $quater = DB::table('review_quaters')->where('id',$quater_id)->where('is_active',true)->first();
    if (count($quater)) {
        //check if anazo data
        //check kajaza zote au subordinates wamesubmit
        //check hajasubmit bado
        //kucheki kama haja submit za idara zote
      $check_workflw =  WorkflowSubmission::where('user_id',$user->id)->where('fy_id',$fy_id)->where('module_id',$this->module_id)->where('quarter_id',$quater_id)->first();
      $return = count($check_workflw) ? true : false;
    }
  }

  if ($user->designation == 'DFPI' && $user->id !=$workflow->user_id) {

    $tier_two_b_users = User::whereBetween('responsible_id',[2,11])->get();
    $quaters = DB::table('review_quaters')->get();
    return view('backend.actionplan.cordinator.consolidater')
    ->with('quaters',$quaters)->with('tier_two_b_users',$tier_two_b_users)->with('workflow',$workflow);
  }

  elseif($user->hasRole('dfpi')){
    $tier_two_b_users = User::where('designation','DFPI',$user->id)->get();
    $quaters = DB::table('review_quaters')->get();
    return view('backend.actionplan.cordinator.consolidater')
    ->with('quaters',$quaters)->with('tier_two_b_users',$tier_two_b_users)->with('workflow',$workflow);
  }



  if ($user->designation('prm')) {
    $role_id = 6;
    $supervisor = User::where('designation','DFPI')->first();
  }

'{!! url("actionplan/datatable/") !!}/'+fy_id+'/'+department_id,

'{!! url("actionplan/revenue_datatable/") !!}/'+fy_id+'/'+department_id,

  if (user_id && id_quater && fny_id) {
    $('#btnModalWorkflow').attr('disabled',true);
    let l = Ladda.create(this);
    l.start();
    viewWorkflow(user_id,fny_id,id_quater);
    l.stop();
  }
});


$(document).ajaxComplete(function() {
      
      $('.update_budget_spent').editable({
        step: 'any',
        params: function(params) {
          return params;
        },
        url: '{{route('actionplan.update_budget')}}',
        success: function (data, response) {
          toastr["success"]("Success! Total budget spent saved", "Dear! {{Auth::user()->firstname}}");
        },
        error: function () {
          toastr["error"]("Request Failed to save total budget spent! An error has occured", "Sorry! {{Auth::user()->firstname}}");
        }
      });
    });






 function showWorkflowButton(fny_id,quater_id) {
        $.ajax({
          url : '{!! url("actionplan/consolidated_workflow_expenditure_button/") !!}/'+fny_id+'/'+quater_id,
          type: 'GET',
          datatype : 'json',
          success:function(data){
            console.log(data);
            console.log('hellow');
            if (data.success) {
              btnModalWorkflow.removeClass('d-none');
            }else{
              btnModalWorkflow.addClass('d-none');
            }
          }
        }); 
      }



        function viewWorkflow(user_id,fny_id,quater_id) {
            $('#btnModalWorkflow').attr('disabled',false);
            $('#AddFields').empty();
            $.ajax({
              url : '{!! url("actionplan/consolidated_expenditure_workflow/") !!}/'+fny_id+'/'+quater_id,
              type : 'GET',
              dataType: 'json',
              success:function(data){
                if (data.success) {
                  $('#modalWorkflow').modal('show');
                  let a = data.data;
                  if (a && a.length) {
                    a.forEach(function(entry) {
                      let status_span = (entry.status == 0) ? '<span class="badge badge-warning badge-small" data-toggle="tooltip" data-placement="top" title="Pending">&nbsp;</span>' : '<span class="h6 badge badge-success badge-small" data-toggle="tooltip" data-placement="top" title="Complete">&nbsp;</span>';
                      let comments = (entry.comments != null) ? "<p>Comments&nbsp;:&nbsp;<q>"+entry.comments+"</q></p>" : '';
                      $('#AddFields').append('<li class="list-group-item"><div class="float-xs-left p-1"><strong><p>'+entry.name+'</strong> &nbsp; '+status_span+'</p> <p>Remark&nbsp;:&nbsp;'+entry.remarks+'</p>'+comments+' <p><small>Workflow Date&nbsp;:&nbsp;<span class="underline">'+moment(entry.workflow_date).format('Do MMM, YYYY h:mm:ss a')+'</span></p></small></div></li>');
                    });
                  }else {
                    $('#AddFields').append('<li class="list-group-item text-danger"><p>No Workflow Initiated</p></li>');
                  }
                } else {
                  toastr["error"]("An error has occured", "Sorry! {{Auth::user()->firstname}}");
                }
              }
            });
          }








  
  public function datatableRevenueImplementation($fy_id,$quarter_id,$department_id,$unit_id=null)
  { 

    dump($department_id);
    $revenue_implementation = ActionIncomeImplementations::select('action_incomes.objective','action_incomes.target','action_incomes.annual_planned_activity','action_incomes.unit','units.alias as section','action_incomes.unit_id as unitId','action_income_implementations.id as id','target_in_tzs','implemented_in_tzs','implementation_rate','reason_for_variance_or_remark','total_implementation_rate','variance_or_remark')
    ->join('quarters','action_income_implementations.quarter_id','=','quarters.id')
    ->join('action_incomes','action_income_implementations.action_income_id','=','action_incomes.id')
    ->join('units','action_incomes.unit_id','=','units.id')
    ->where('quarter_id',$quarter_id)
    ->where('financial_year_id',$fy_id);
    dd($revenue_implementation);
    if (!empty($unit_id)) {
      $revenue_implementation->where('unit_id',$unit_id);
    } else {
      $units = Unit::where('department_id',$department_id)->pluck('name');
      $revenue_implementation->whereIn('unit',$units);
    }
    $revenue_implementation->orderBy('unit_id','DESC')->orderBy('action_income_implementations.id','ASC');
    $can_update = $this->canUpdateRevenueImplementation($fy_id,$quarter_id,$unit_id);
    return DataTables::of($revenue_implementation)
    ->editColumn("implemented_in_tzs", function($data) use($can_update) {
      $implemented_in_tzs = !empty($data->implemented_in_tzs) ? number_format($data->implemented_in_tzs,2) : $data->implemented_in_tzs;
      return $can_update ?  '<a href="" class="update_imple_tzs money" data-name="implemented_in_tzs" data-type="text" data-min="0" data-pk="'.$data->id.'" >'.$implemented_in_tzs.'</a>' : $implemented_in_tzs;   
    })->editColumn("implementation_rate", function($data) use($can_update) {
      return  !empty($data->implementation_rate) ? round($data->implementation_rate,2).'%' : $data->implementation_rate;
    })->editColumn("reason_for_variance_or_remark", function($data) use($can_update) {
      if($data->implementation_rate == 0 && $data->implementation_rate != null){
        return '';  
      }else{
        return $can_update ?  '<a href="" class="update_reason_for_variance" data-name="reason_for_variance_or_remark" data-type="textarea" data-pk="'.$data->id.'" >'.$data->reason_for_variance_or_remark.'</a>' : $data->reason_for_variance_or_remark;
      }
    })->editColumn("total_implementation_rate", function($data) use($can_update) {
      return  !empty($data->total_implementation_rate) ? round($data->total_implementation_rate,2).'%' : $data->total_implementation_rate;
    })->editColumn("variance_or_remark", function($data) use($can_update) {
      if($data->total_implementation_rate == 0 && $data->total_implementation_rate != null){
        return '';  
      }else{
        return $can_update ?  '<a href="" class="update_variance_remark" data-name="variance_or_remark" data-type="textarea" data-pk="'.$data->id.'" >'.$data->variance_or_remark.'</a>' : $data->variance_or_remark;
      }
    })
    ->editColumn("target_in_tzs", function($data){
      return number_format($data->target_in_tzs,2);
    })
    ->rawColumns(['implemented_in_tzs','implementation_rate','total_implementation_rate','variance_remark'])->make(true);
  } 




  dump($unit_id);       
  $actionrevenue = ActionIncome::select('unit','objective','target','annual_planned_activity','total_approved_budget_revenue_for','total_actual_collection_for','action_income_implementations.total_implementation_rate','action_income_implementations.financial_year_id as ID','quarters.id')
  ->join('units','action_incomes.unit_id','=','units.id')
  ->join('action_income_implementations','action_income_implementations.action_income_id','=','action_incomes.id')
  ->join('quarters','action_income_implementations.quarter_id','=','quarters.id')
  ->where('quarters.id',$quarter_id)
  ->where('action_incomes.financial_year_id',$fy_id);
    // dump($actionrevenue->get());
  if (!empty($unit_id)) {
    $actionrevenue->where('unit',$unit_id);
    dump('hellow');
  } else {
    dump('aaaa');
    dump($department_id);
    $units = Unit::where('department_id',$department_id)->pluck('name');
    $actionrevenue->whereIn('unit',$units);
    dump($units);
    dump($actionrevenue->get());
  }
  dump($actionrevenue->get());
  return DataTables::of($actionrevenue)
  ->editColumn("total_approved_budget_revenue_for", function($data){
    return number_format($data->total_approved_budget_revenue_for,2);
  })->editColumn("total_actual_collection_for", function($data){
    return number_format($data->total_actual_collection_for,2);
  })->editColumn("total_implementation_rate", function($data) use($can_update) {
    return  !empty($data->total_implementation_rate) ? round($data->total_implementation_rate,2).'%' : $data->total_implementation_rate;
  })->make(true); 









  <ul class="tabs-animated-shadow tabs-animated nav">
  <li class="nav-item">
  <a role="tab" class="switchTab nav-link show active tab" id="tab-c-0" data-toggle="tab" href="#tabOne" aria-selected="false">
  <span>Actionplan Expenditure</span>
  </a>
  </li>
  <li class="nav-item">
  <a role="tab" class="switchTab nav-link show tab" id="tab-c-1" data-toggle="tab" href="#tabTwo" aria-selected="false">
  <span>Implementation Expenditure</span>
  </a>
  </li>
  <li class="nav-item">
  <a role="tab" class="switchTab nav-link show active tab" id="tab-c-0" data-toggle="tab" href="#tabThree" aria-selected="false">
  <span>ActionPlan Revenue</span>
  </a>
  </li>
  <li class="nav-item">
  <a role="tab" class="switchTab nav-link show tab" id="tab-c-1" data-toggle="tab" href="#tabFour" aria-selected="false">
  <span>Implementation Revenue</span>
  </a>
  </li>
  </ul>



  >where('unit_id',$unit_id)
  ->where('action_incomes.financial_year_id',$fy_id);

  if (!empty($unit_id)) {
    $actionrevenue->where('units.id',$unit_id);
  } else {
    $units = Unit::where('department_id',$department_id)->pluck('name');
    $actionrevenue->whereIn('unit',$units);
      // $actionrevenue->whereIn('Unit',$Unit_id)->orderBy('quarter_id','DESC');
      // 
  }

  switch($request->name) {
    case 'total_actual_collection_for':
    $total_collection = ActionIncomeImplementations::find($request->pk);
    $implementation__rate_calculation = $total_collection->target_in_tzs - $request->value;
    $total_implementation_rate = ($implementation__rate_calculation / $total_collection->target_in_tzs) * 100;
                // dd($total_implementation_rate);
    $input = [
      'total_actual_collection_for'  => $request->value,
      'updated_at' => Carbon::now(),
      'total_implementation_rate' => $total_implementation_rate 
    ];
    break;
    case 'variance_or_remark':    
    $input = [
      'variance_or_remark'  => $request->value,
      'updated_at' => Carbon::now()
    ]; 
    break;
    default:
    $input = [];
    break;





    <div class="card-header">
    <span class="idara"></span> &nbsp;<span class="t1Year"> </span> <span class="quater_imp"></span>
    <span class="quater_review"> </span> &nbsp; <span class="unit_name"> </span>
    <div class="btn-actions-pane-right text-capitalize mt-2 mb-2"> 
    <button  class="btn btn-md btn-info btn-shadow d-none" id="btnTierTwoModalWorkflow"> 
    <i class="fa fa-random"></i> View Workflow 
    </button> 
    @if(Auth::user()->can('consolidate perfomance review'))           
    <button  class="btn btn-md btn-warning btn-shadow d-none" id="ReverseTierTwo"> 
    <i class="fa fa-reply-all"></i> Reverse Consolidated Perfomance
    </button> 
    <button  class="btn btn-md btn-success d-none" id="btnTierTwoWorkflowApprove"> 
    <i class="fa fa-check"></i> Consolidate <span class="t1Year"> </span>&nbsp;<span class="quater_result"></span>
    </button>
    <button  class="btn btn-md btn-warning d-none" id="btnTierTwoWorkflowReject"> 
    <i class="fa fa-reply-all"></i> Return <span class="t1Year"> </span>&nbsp;<span class="quater_result"></span>
    </button>
    @endif
    @if(Auth::user()->can('approve consolidated perfomance review'))
    <button  class="btn btn-md btn-warning btn-shadow d-none" id="DfpiReverseTierTwo"> 
    <i class="fa fa-reply-all"></i> Reverse 
    </button>   
    @endif
    </div>  
    </div>