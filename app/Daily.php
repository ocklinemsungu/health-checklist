<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Daily extends Model
{
    protected $table ='daily';

    protected $fillable =[
            'service_name','inspected_by','signature'
    ];

    protected $primaryKey ='id';

   //Timestample
   public $timestamps   ='true';

   public function maintenance()
   {           
   return $this->belongTo('App\Weakly'); 
    }
}
