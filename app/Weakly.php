<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Weakly extends Model
{
     protected $table ='weakly';

    protected $fillable =[
            'service_name','inspected_by','signature'
    ];

    protected $primaryKey ='id';

   //Timestample
   public $timestamps   ='true';

   public function maintenance()
   {           
   return $this->hasMany('App\daily'); 
    }
}
