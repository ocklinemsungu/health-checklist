<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Collective\Html\Eloquent\FormAccessible;

use App\Daily;
use App\Weakly;
use DB;
use Auth;
use Carbon;
use config;
use Excel;


class DailyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function DailyDetail()
    {
        $checks = Daily::all();
        
        return view('Daily/days',compact('checks'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
          // dd($request->all());
      $this->validate($request, [
        'status'  => 'required',
        // 'comment'  => 'required',
        'inspected_by'  => 'required',
        'signature' => 'required'

    ]);
     $message = [
        'status'  => 'kindly fill name',
        'comment'  => 'kindly fill comment',
        'inspected_by'  => 'kindly fill inspected by',
        'signature'  => 'kindly fill  inspector signature'

    ];
 // dd('hellow');
    $check = new Daily;
    $check->status = $request->input('status');
    // $check->status = $request->input('status');
    $check->comment = $request->input('comment');
    $check->inspected_by = $request->input('inspected_by');
    $check->signature = $request->input('signature');

    // dd('hellow');
    $check->save();

      return view('Daily/days')->with('check have been updated');
}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
