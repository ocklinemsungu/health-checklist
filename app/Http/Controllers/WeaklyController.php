<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Weakly;
use DB;
use Config;

class WeaklyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function WeaklyDetail()
    {
        
        return view('Weakly.weakly');
        // ->with($weakly_checkup,'weakly_checkup');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $weakly_checkup = new Weakly();
        return view('weakly.create',compact($weakly_checkup,'weakly_checkup'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return 123445;

        $this->validate($request, [
            'name'  => 'required',
            'status'  => 'required',
            'description'  => 'required'
        ]);

        $message = [
            'name'  => 'kindly fill name',
            'status'  => 'kindly fill status',
            'description'  => 'kindly fill description'
            
        ];

        $weakly = new weakly;

        $weakly->name = $request->input('name');
        $weakly->status = $request->input('status');
        $weakly->description = $request->input('description');
        $weakly->daily_id = $request->input('daily_id');

        $weakly->save();

        $this->mail();

        \Session::flash('flash_message', 'Task successfully added!');

        return redirect()->route('weakly')->with('success','weakly success Created');
    }
    public function mail()
    {
        $weakly = (['ockline','Samweli']);
       Mail::to('ocklinemsungu@gmail.com')
       ->cc('ockline.msungu@gmail.com','ocklinemwanzelu@gmail.com')->send(new NewUserNotification($weakly));
   }       


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
