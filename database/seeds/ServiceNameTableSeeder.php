<?php

use Illuminate\Database\Seeder;

class ServiceNameTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
      \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
    	\DB::table('service_name')->truncate();
    	\DB::statement('SET FOREIGN_KEY_CHECKS=1;');  
    
       \DB::table('service_name')->insert(array (
    		0 => 
    		array (
    			'id' => 1,
                'name' => 'vCenter Server',
                'description' => 'Vitual Machine',
                'location' => 'HQ',
                'categories_id' => '2.1',
                'unit_id' => null,
                'created_at' =>'2020-11-11 10:30:00',
                'updated_at' =>'2020-11-11 10:30:00',
            ),
            1 => 
    		array (
    			'id' => 2,
                'name' => 'Internet',
                'description' => 'Vitual Machine',
                'location' => 'HQ',
                'categories_id' => '2.2',
                'unit_id' => null,
                'created_at' =>'2020-11-11 10:30:00',
                'updated_at' =>'2020-11-11 10:30:00',
            ),
           2 => 
    		array (
    			'id' => 3,
                'name' => 'DHCP and DNS',
                'description' => 'Vitual Machine',
                'location' => 'HQ',
                'categories_id' => '2.3',
                'unit_id' => null,
                'created_at' =>'2020-11-11 10:30:00',
                'updated_at' =>'2020-11-11 10:30:00',
            ),
            3 => 
    		array (
    			'id' => 4,
                'name' => 'Document',
                'description' => 'Vitual Machine',
                'location' => 'HQ',
                'categories_id' => '2.4',
                'unit_id' => null,
                'created_at' =>'2020-11-11 10:30:00',
                'updated_at' =>'2020-11-11 10:30:00',
            ),
           4 => 
    		array (
    			'id' => 5,
                'name' => 'Send and Receive internal email',
                'description' => 'mail.google.com',
                'location' => 'HQ',
                'categories_id' => '2.5',
                'unit_id' => null,
                'created_at' =>'2020-11-11 10:30:00',
                'updated_at' =>'2020-11-11 10:30:00',
            ),
            5 => 
    		array (
    			'id' => 6,
                'name' => 'Send and Receive external email',
                'description' => 'mail',
                'location' => 'HQ',
                'categories_id' => '2.6',
                'unit_id' => null,
                'created_at' =>'2020-11-11 10:30:00',
                'updated_at' =>'2020-11-11 10:30:00',
            ),
           6 => 
    		array (
    			'id' => 7,
                'name' => 'MAC',
                'description' => 'Members and Claim Administration system',
                'location' => 'HQ',
                'categories_id' => '2.7',
                'unit_id' => null,
                'created_at' =>'2020-11-11 10:30:00',
                'updated_at' =>'2020-11-11 10:30:00',
            ),
            7 => 
    		array (
    			'id' => 8,
                'name' => 'e-Office',
                'description' => 'Electronic Office',
                'location' => 'HQ',
                'categories_id' => '2.8',
                'unit_id' => null,
                'created_at' =>'2020-11-11 10:30:00',
                'updated_at' =>'2020-11-11 10:30:00',
            ),
           8 => 
    		array (
    			'id' => 9,
                'name' => 'Intranet',
                'description' => 'Internal Network',
                'location' => 'HQ',
                'categories_id' => '2.9',
                'unit_id' => null,
                'created_at' =>'2020-11-11 10:30:00',
                'updated_at' =>'2020-11-11 10:30:00',
            ),
            9 => 
    		array (
    			'id' => 10,
                'name' => 'Public Website',
                'description' => 'WCF website',
                'location' => 'HQ',
                'categories_id' => '2.10',
                'unit_id' => null,
                'created_at' =>'2020-11-11 10:30:00',
                'updated_at' =>'2020-11-11 10:30:00',
            ),
           10 => 
    		array (
    			'id' => 11,
                'name' => 'WCFePG',
                'description' => 'WCF Portal',
                'location' => 'HQ',
                'categories_id' => '2.11',
                'unit_id' => null,
                'created_at' =>'2020-11-11 10:30:00',
                'updated_at' =>'2020-11-11 10:30:00',
            ),
            11 => 
    		array (
    			'id' => 12,
                'name' => 'Abacus/HRS',
                'description' => 'Payroll System',
                'location' => 'HQ',
                'categories_id' => '2.12',
                'unit_id' => null,
                'created_at' =>'2020-11-11 10:30:00',
                'updated_at' =>'2020-11-11 10:30:00',
            ),
           12 => 
    		array (
    			'id' => 13,
                'name' => 'ERP',
                'description' => 'Enteprise Resource Planning System',
                'location' => 'HQ',
                'categories_id' => '2.13',
                'unit_id' => null,
                'created_at' =>'2020-11-11 10:30:00',
                'updated_at' =>'2020-11-11 10:30:00',
            ),
            13 => 
    		array (
    			'id' => 14,
                'name' => 'Time and Attendance System',
                'description' => 'Biometric Machine',
                'location' => 'HQ',
                'categories_id' => '2.14',
                'unit_id' => null,
                'created_at' =>'2020-11-11 10:30:00',
                'updated_at' =>'2020-11-11 10:30:00',
            ),
           14 => 
    		array (
    			'id' => 15,
                'name' => 'Air Condition(AC)',
                'description' => 'Air Condion For Server room',
                'location' => 'HQ',
                'categories_id' => '1.1',
                'unit_id' => null,
                'created_at' =>'2020-11-11 10:30:00',
                'updated_at' =>'2020-11-11 10:30:00',
            ),
            15 => 
    		array (
    			'id' => 16,
                'name' => 'Server Room UPS',
                'description' => 'Air Condion For Server room',
                'location' => 'HQ',
                'categories_id' => '1.2',
                'unit_id' => null,
                'created_at' =>'2020-11-11 10:30:00',
                'updated_at' =>'2020-11-11 10:30:00',
            ),
           16 => 
    		array (
    			'id' => 17,
                'name' => 'Air Condition(AC)',
                'description' => 'Air Condion For Server room',
                'location' => 'Dodoma',
                'categories_id' => '1.3',
                'unit_id' => null,
                'created_at' =>'2020-11-11 10:30:00',
                'updated_at' =>'2020-11-11 10:30:00',
            ),
            17 => 
    		array (
    			'id' => 18,
                'name' => 'Server Room UPS',
                'description' => 'Air Condion For Server room',
                'location' => 'Dodoma',
                'categories_id' => '1.4',
                'unit_id' => null,
                'created_at' =>'2020-11-11 10:30:00',
                'updated_at' =>'2020-11-11 10:30:00',
            ),
        ));
   $this->command->info('ServicesName added successfully');

    }
}
