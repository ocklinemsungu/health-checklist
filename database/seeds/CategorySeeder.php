<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
    	\DB::table('categories')->truncate();
    	\DB::statement('SET FOREIGN_KEY_CHECKS=1;');  
    
       \DB::table('categories')->insert(array (
    		0 => 
    		array (
    			'id' => 1,
                'name' => 'General Condition',
                'description' => 'General Condition of Server Room',
                'created_at' =>'2020-11-11 10:30:00',
                'updated_at' =>'2020-11-11 10:30:00',
            ),
            1 => 
    		array (
    			'id' => 2,
                'name' => 'Availability of Service',
                'description' => 'Availability Of Service or System',
                'created_at' =>'2020-11-11 10:30:00',
                'updated_at' =>'2020-11-11 10:30:00',
            ),
             ));
   $this->command->info('Categories added successfully');
    }
}
