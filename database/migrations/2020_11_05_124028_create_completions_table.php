<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompletionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('completions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('type');
            $table->string('name');
            $table->string('status');
            $table->string('comment');
            $table->string('image')->nullable();
            $table->string('site_serve')->nullable();
            $table->string('inspected_by');
            $table->string('signature')->nullable();
            $table->string('verified_by');
            $table->string('verifer_signature')->nullable();
            $table->time('Date_time')->nullable();
            $table->integer('daily_id')->nullable();
            $table->integer('weakly_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('completions');
    }
}
